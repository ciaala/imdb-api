/**
 * Created by crypt on 05/05/17.
 */
//var console = {log: function(message) { process.stdout.write(message); }};

var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var assert = require('assert');
function getJSON(url) {
    "use strict";

    return new Promise((resolve, reject) => {
        const async = true;
        const request = new XMLHttpRequest();
        console.log('Connecting to ' + url);
        request.open('GET', url);
        request.onload = function () {
            try {
                if (this.status === 200) {
                    resolve(JSON.parse(this.responseText));

                } else {
                    reject(this.status, this.statusText);
                }
            } catch (e) {
                console.log(e);
                reject(-1, e.message);
            }
        };
        request.onerror = function () {
            reject(this.status, this.statusText);
        };
        request.send();
    });
}
function Rarbg() {
    function assert(value) {
        if (value !== true) {
            console.error('Invalid');
        }
    }

    var tables = document.getElementsByClassName('lista2t');
    assert(tables.length > 0);
    var table = tables[0];
    var rows = table.getElementsByClassName('lista2');

    assert(rows.length > 0);
    for (var i = 0; i < rows.length; i++) {
        var cell = rows[i].cells[1];
        var $title = cell.childNodes[0].textContent;
        var rawGenreVote = cell.childNodes[cell.childNodes.length - 2].textContent.split(" IMDB: ");
        var $vote = parseFloat(rawGenreVote[1]);
        var $genres = rawGenreVote[0].split(', ');
        var $genreString = rawGenreVote[0];
        var $movie = {title: $title, vote: $vote, genres: $genres, genreString: $genreString}

        var isStyled = false;
        if ($genres.includes('Horror') && $vote > 5.0) {
            cell.style.backgroundColor = 'pink';
            isStyled = true;
        }
        if ($genres.includes('Sci-Fi') && $vote > 5.0) {
            cell.style.backgroundColor = 'cyan';
            isStyled = true;
        }
        if ($vote > 8.0) {
            cell.style.backgroundColor = '#b0FFb0';
            isStyled = true;
        } else if ($vote >= 7.2) {
            cell.style.backgroundColor = 'orange';
            isStyled = true;
        } else if ($vote >= 7.0) {
            cell.style.backgroundColor = '#FFFFb0';
            isStyled = true;
        }
        if (isStyled === false) {
            cell.style.backgroundColor = 'rgb(231, 243, 251)';
        } else if (isStyled === true) {
            console.log($movie);
        }
    }
}
function ImdbService() {
    "use strict";
    const baseUrl = 'http://www.omdbapi.com/?';

    function makeRequest(parameters, handler) {
        let parametersString = "";
        for (const key in parameters) {
            parametersString += "&" + key + "=" + parameters[key];
        }
        getJSON(baseUrl + parametersString).then(response => {
            assert(response !== null && response !== undefined, "Movie obtained");
            handler(response);
        }).catch(
            e => console.log("Should not happen: " + e)
        );
    }

    this.getTitleRating = function(title, year, type) {

        const parameters = {
            r: 'json',
            v: 1,
            plot: 'short'
        };
        if (title !== undefined && title !== null) {
            parameters['t'] = title;

            if (type !== undefined && type !== null) {
                parameters['type'] = type;
            }
            if (year !== undefined && year !== null) {
                parameters['y'] = year;
            }
        }
        makeRequest(parameters, (response) => {return response;})
    };
    this.getYearBestMovie = function(year) {
        if (year !== undefined && year !== null) {

            const parameters = {
                r: 'json',
                v: 1,
                plot: 'short'
            };
            makeRequest(parameters, {});
        }
        return 'please provide the year';
    };
}

const imdb = new ImdbService();
console.log(imdb.getTitleRating('Prometheus', '2012'));

console.log(imdb.getYearBestMovie(2012));

